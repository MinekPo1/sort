#include <vector>
#include <iostream>
#include <time.h>
#include <random>
#include <stdio.h>

using namespace std;

void sortowanie_babelkowe_0(vector<int> &wektor) {
    // zmienna pomocnicza, pozwalająca nam zauważyć kiedy wektor jest posortowany
    bool zrobilismy_cos = true;
    while (zrobilismy_cos) {
        // ustawiamy zmienną pomocniczą na fałsz. Jeżeli nie będzie zmieniona,
        // Funkcja będzie zakończona
        zrobilismy_cos = false;
        // iterujemy na wektorze:
        // i "idzie" od zera do indeksu przedostatniego elementu
        for (int i = 0; i < wektor.size() - 1; i++) {
            // sprawdzamy czy dwa elementy nie są w poprawnej kolejności
            if (wektor[i] > wektor[i+1]) {
                //printf("%i: %i<->%i\n", i, wektor[i], wektor[i+1]);
                // zamieniamy
                int temp = wektor[i];
                wektor[i] = wektor[i+1];
                wektor[i+1] = temp;
                // Gdyż przestawiliśmy dwa elementy, musimy sprawdzić
                // wektor ponownie, gdyż element który teraz znajduje się
                // w wektor[i] może być mniejszy od wektor[i-1] (jeżeli i > 0)
                zrobilismy_cos = true;
            }
        }
        // jeżeli zrobilismy_cos nie zostało zmienione na prawdę, wyjdziemy z pętli,
        // a potem z funkcji
    }
}

void sortowanie_babelkowe_1(vector<int> &wektor) {
    bool zrobilismy_cos = true;
    int j = 0; // ilość już posortowanych elementów
    while (zrobilismy_cos) {
        zrobilismy_cos = false;
        // teraz nie będziemy iterować po j elementach
        // od końca, które wiemy że są posortowane
        for (int i = 0; i < wektor.size() - 1 - j; i++) {
            if (wektor[i] > wektor[i+1]) {
                int temp = wektor[i];
                wektor[i] = wektor[i+1];
                wektor[i+1] = temp;
                zrobilismy_cos = true;
            }
        }
        // wiemy że przesunęliśmy **przynajmniej** jeden element
        // na odpowiednie miejsce
        j++;
    }
}

void sortowanie_przez_wstawianie(vector<int> &wektor) {
    // nie iterujemy po pierwszym elemencie,
    // gdyż będziemy przesuwać elementy w "dół"
    for (int i = 1; i < wektor.size(); i ++) {
      // wyciągamy element z wektora
      int temp = wektor[i];
      wektor.erase(wektor.begin() + i); // patrz dodatek 1

      // szukamy gdzie chcemy wstawić element
      // należy zauważyć, że i-ty element jest teraz inny
      // od tego którego wyciągnęliśmy.
      
      // deklarujemy j poza pętlą, by użyć go potem.
      // chcemy by j było nowym indeksem elementu
      int j;
      for (j = 0; j < i; j ++) {
            // jeżeli element na pozycji j jest mniejszy od wcześniej wyjętego
            // elementu, chcemy wstawić element po nim
            if (wektor[j] >= temp) {

                break;
            }
      }

      // wstawiamy element z powrotem do wektora
      wektor.insert(wektor.begin()+j, temp); // patrz dodatek 1

    }
}

void sortowanie_przez_wybieranie_0(vector<int> &wektor) {
    vector<int> wektor2;

    // przenosimy wszystkie elementy do drugiego wektora
    // więc wyjmujemy elementy z pierwszego wektora
    // dopóki ten jest pusty
    while(! wektor.empty() ) {
        // szukamy najmniejszego elementu w wektorze
        int fmin = 0;
        // zaczynamy od drugiego elementu
        for(int i = 1; i < wektor.size(); i++) {
            // sprawdzamy czy znaleźliśmy nowy najmniejszy element
            if (wektor[fmin] > wektor[i]) {
                fmin = i;
            }
        }
        // przekładamy najmniejszy element na koniec drugiego wektora
        wektor2.push_back(wektor[fmin]);
        wektor.erase(wektor.begin()+fmin); // patrz dodatek 1
    }
    // mamy posortowane elementy w drugim wektorze, jednak chcemy je
    // w pierwszym. Dlatego, zamieniamy je.
    wektor.swap(wektor2);
}

void sortowanie_przez_wybieranie_1(vector<int> &wektor) {

    // j wyznacza długość posortowanej części wektora
    for(int j = 0; j < wektor.size() - 1; j++ ) {
        // szukamy najmniejszego elementu w nieposortowanej części wektora
        int fmin = j;
        // zaczynamy od drugiego elementu
        for(int i = j+1; i < wektor.size(); i++) {
            // sprawdzamy czy znaleźliśmy nowy najmniejszy element
            if (wektor[fmin] > wektor[i]) {
                fmin = i;
            }
        }
        // przekładamy najmniejszy element na początek, za poprzedni element
        int tmp = wektor[j];
        wektor[j] = wektor[fmin];
        wektor[fmin] = tmp;
    }
}

template <int bits = 4>
void sortowanie_matrix(vector<int> &wektor) {
    vector<int> wektor2;

    for (int j = 0; j < wektor.size(); j++)
      wektor2.push_back(0);

    for (int i = 0; i < sizeof(int) * 8/bits; i++) {
        int licznik[1<<bits] = {0};

        for (int j = 0; j < wektor.size(); j++)
            licznik[(wektor[j] >> i * bits) & ((1<<bits)-1)] ++;

        int sum = 0;
        for (int j = 0; j < (1<<bits); j++)
            licznik[j] = (sum += licznik[j]) - licznik[j];

        for (int j = 0; j < wektor.size(); j++)
            wektor2[licznik[(wektor[j] >> i * bits) & ((1<<bits)-1)] ++] = wektor[j];

        wektor.swap(wektor2);
    }
}

void sortowanie_standardowe(vector<int> &wektor) {
    sort(wektor.begin(), wektor.end());
}

namespace Testowanie {
  // zwraca liczbę i, a <= i < b
  int randint(int a, int b) {
      return rand() % (b - a) + a;
  }

  void potasuj(vector<int> &wektor) {
      for (int i = 0; i < wektor.size()-1; i++) {
          int j = randint(i+1, wektor.size());
          int temp = wektor[i];
          wektor[i] = wektor[j];
          wektor[j] = temp;
      }
  }

#ifndef TEST_ITER_COUNT
#define TEST_ITER_COUNT 100
#endif
#ifndef TEST_RANGE_SIZE
#define TEST_RANGE_SIZE 100
#endif

  const string time_units[]  = {" s", "ms", "μs"};
  const int anim_n_frames = 4;
  const int anim_freq = 2;
  const string anim_frames[anim_n_frames] = {
      "[/]", "[-]",
      "[\\]","[|]",
  };

  void dtime(int ti, int c = TEST_ITER_COUNT) {
      float t = float(ti) / c;
      t /= CLOCKS_PER_SEC;
      int i = 0;
      while (t < 1 and i < 2) {
        t *= 1000;
        i ++;
      }

      printf("%5.1f %s", t, time_units[i].c_str());
  }

  int test(void funkcja_sortujaca(vector<int>&), string logname = "???") {
      vector<int> wektor;
      for(int i = 0; i < TEST_RANGE_SIZE; i++)
        wektor.push_back(i);

      int ttime = 0;
      int log_step = CLOCKS_PER_SEC/anim_freq/anim_n_frames;

      int last_step = 0;

      for(int i = 0; i < TEST_ITER_COUNT; i++) {
            if ((ttime - last_step) >= log_step ) {
                last_step = ttime;
                printf(
                    "%-30s\t %2i%% %s ~",
                    (logname+":").c_str(),
                    i*100/TEST_ITER_COUNT,
                    anim_frames[(ttime*anim_freq*anim_n_frames/CLOCKS_PER_SEC)%anim_n_frames].c_str()
                );
                dtime(ttime, i);
                printf(" ETA ");
                long eta = long(ttime) * long(TEST_ITER_COUNT-i) / i;
                dtime(eta, 1);
                printf("\r");
                flush(cout);
          }

          potasuj(wektor);

          int stime = clock();
          funkcja_sortujaca(wektor);
          int etime = clock();
          ttime += etime - stime;

          for (int j = 0; j < TEST_RANGE_SIZE; j++)
              if (wektor[j] != j) {
                  return -1;
              }
      }

      return ttime;
  }

  void show_test_result(int test_outp, string logname) {
      if (test_outp == -1) {
        printf("%-30s\t błędny wynik!                     \n", (logname+":").c_str());
        return;
      }

      printf("%-30s\t średnio ~", (logname+":").c_str());
      dtime(test_outp);
      printf(" ( ");
      dtime(test_outp, 1);
      printf(" )\n");
  }
}

int main() {
    srand(time(nullptr));
    int t;
    #ifndef TEST_SHORT_MATRIX_W
    #define TEST_SHORT_MATRIX_W 4
    #endif
    #ifndef TEST_LONG_MATRIX_W
    #define TEST_LONG_MATRIX_W 8
    #endif

    #ifndef TEST_NO_BUBBLE
    #ifndef TEST_NO_SLOW_BUBBLE
    t = Testowanie::test(sortowanie_babelkowe_0, "sortowanie bąbelkowe 0");
    Testowanie::show_test_result(t, "sortowanie bąbelkowe 0");
    #endif
    #ifndef TEST_NO_FAST_BUBBLE
    t = Testowanie::test(sortowanie_babelkowe_1, "sortowanie bąbelkowe 1");
    Testowanie::show_test_result(t, "sortowanie bąbelkowe 1");
    #endif
    #endif
    #ifndef TEST_NO_INSERT
    t = Testowanie::test(sortowanie_przez_wstawianie, "sortowanie przez wstawianie");
    Testowanie::show_test_result(t, "sortowanie przez wstawianie");
    #endif
    #ifndef TEST_NO_SELECTION
    #ifndef TEST_NO_SELECTION_0
    t = Testowanie::test(sortowanie_przez_wybieranie_0, "sortowanie przez wybieranie 0");
    Testowanie::show_test_result(t, "sortowanie przez wybieranie 0");
    #endif
    #ifndef TEST_NO_SELECTION_1
    t = Testowanie::test(sortowanie_przez_wybieranie_1, "sortowanie przez wybieranie 1");
    Testowanie::show_test_result(t, "sortowanie przez wybieranie 1");
    #endif
    #endif
    #ifndef TEST_NO_ADVANCED
    #ifndef TEST_NO_MATRIX
    #ifndef TEST_NO_MATRIX_SHORT
    t = Testowanie::test(sortowanie_matrix<4>, "sortowanie matrix (S)");
    Testowanie::show_test_result(t, "sortowanie matrix (S)");
    #endif
    #ifndef TEST_NO_MATRIX_LONG
    t = Testowanie::test(sortowanie_matrix<8>, "sortowanie matrix (L)");
    Testowanie::show_test_result(t, "sortowanie matrix (L)");
    #endif
    #endif
    #ifndef TEST_NO_STD
    t = Testowanie::test(sortowanie_standardowe, "sortowanie standardowe");
    Testowanie::show_test_result(t, "sortowanie standardowe");
    #endif
    #endif
}
