# Sortowanie

W tym pliku bardziej skupiam się na *jak* algorytm działa, lecz także pokaże przykładową implementacje.

W każdej implementacji (c++) funkcja sortująca będzie operowała na wektorze z elementami typu `int`.
Zakładam też że długość wektora "mieści się" w typie `int` (ie nie jest > od 2 147 483 648).
Sortować będę od najmniejszej do największej.

```c++

void sortowanie_nazwa(vector<int> &wektor) {
    /*
    ...
    */
}

```

Należy zauważyć że argument `wektor` jest poprzedzony znakiem `&`. Dzięki temu wszelkie zmiany na argumencie
będą także obecne po zakończeniu funkcji:

```c++
int main() {
    // tworzymy wektor
    vector<int> dane = {/*...*/};

    sortowanie_nazwa(dane); // dane jest teraz posortowane
}

```

Przypomnę, że w `c++` wektor ma kilka przydatnych funkcji, w tym:

- `.size()`: zwraca ilość elementów wektora
- `[...]`: zwraca wartość na danym indeksie
- `[...] = ...`: ustawia wartość na danym indeksie.
- `.erase(...)`: pozwala na usunięcie elementu z wektora. Patrz dodatek 1.
- `.insert(...)`: pozwala na wstawienie elementu do wektora na danym indeksie. Patrz dodatek 1.
- `.push_back(...)`: jak `.insert(...)`, lecz dodaje element na koniec
- `.swap(..)`: zamienia dwa wektory miejscami

Wektor znajduje się w bibliotece `vector`.

Wraz z tym plikiem publikuje kod źródłowy łączący te trzy algorytmy,
oraz dodatkowo algorytm sortujący matrix i otoczkę wokół funkcji sort z biblioteki standardowej, oraz porównuje ich prędkość.

## Sortowanie bąbelkowe

Sortowanie bąbelkowe jest bardzo proste dla komputera. Polega na iterowaniu po danych, sprawdzając czy sąsiadujące elementy
są w kolejności. Jeżeli nie są, to zamieniamy je. Powoduje to, że największe wartości "wypływają na powierzchnie",
jak bąbelki w wodzie, skąd nazwa.

Jeżeli po "przejściu" przez wektor, nie zmieniliśmy żadnej wartości, możemy zakończyć funkcję, gdyż wektor jest
posortowany.

Najprostsza implementacja algorytmu wygląda następująco:

```c++
void sortowanie_babelkowe_0(vector<int> &wektor) {
    // zmienna pomocnicza, pozwalająca nam zauważyć kiedy wektor jest posortowany
    bool zrobilismy_cos = true;
    while (zrobilismy_cos) {
        // ustawiamy zmienną pomocniczą na fałsz. Jeżeli nie będzie zmieniona,
        // Funkcja będzie zakończona
        zrobilismy_cos = false;
        // iterujemy na wektorze:
        // i "idzie" od zera do indeksu przedostatniego elementu
        for (int i = 0; i < wektor.size() - 1; i++) {
            // sprawdzamy czy dwa elementy nie są w poprawnej kolejności
            if (wektor[i] > wektor[i+1]) {
                // zamieniamy
                int temp = wektor[i];
                wektor[i] = wektor[i+1];
                wektor[i+1] = temp;

                // Gdyż przestawiliśmy dwa elementy, musimy sprawdzić
                // wektor ponownie, gdyż element który teraz znajduje się
                // w wektor[i] może być mniejszy od wektor[i-1] (jeżeli i > 0)
                zrobilismy_cos = true;
            }
        }
        // jeżeli zrobilismy_cos nie zostało zmienione na prawdę, wyjdziemy z pętli,
        // a potem z funkcji
    }
}
```

Można także zauważyć, że po każdym przejściu przez wektor, kolejna największa wartość jest przesunięta na jej odpowiednie miejsce
Możemy to wykorzystać, by przyspieszyć algorytm:

```c++
void sortowanie_babelkowe_1(vector<int> &wektor) {
    bool zrobilismy_cos = true;
    int j = 0; // ilość już posortowanych elementów
    while (zrobilismy_cos) {
        zrobilismy_cos = false;
        // teraz nie będziemy iterować po j elementach
        // od końca, które wiemy że są posortowane
        for (int i = 0; i < wektor.size() - 1 - j; i++) {
            if (wektor[i] > wektor[i+1]) {
                int temp = wektor[i];
                wektor[i] = wektor[i+1];
                wektor[i+1] = temp;
                zrobilismy_cos = true;
            }
        }
        // wiemy że przesunęliśmy **przynajmniej** jeden element
        // na odpowiednie miejsce
        j++;
    }
}
```

Ten algorytm, w najgorszym wypadku robi n^2/2 kroków, co oznacza, że możemy powiedzieć że ma kompleksowość O(n^2).

## Sortowanie przez wstawianie

Sortowanie przez wstawianie jest bardziej intuicyjnym sposobem sortowania dla człowieka.

Polega on na szukanie elementu który ma najmniejszą wartość, po czym wyjmujemy go z nie posortowanego zbioru
i wkładamy na właściwe miejsce.

!!! Należy zauważyć, że jeżeli używamy tablicy, musimy przesunąć każdy element pomiędzy miejscem z którego bierzemy
element, a miejscem gdzie chcemy go umieścić, o jedno miejsce.

```c++
void sortowanie_przez_wstawianie(vector<int> &wektor) {
    // nie iterujemy po pierwszym elemencie,
    // gdyż będziemy przesuwać elementy w "dół"
    for (int i = 1; i < wektor.size(); i ++) {
      // wyciągamy element z wektora
      int temp = wektor[i];
      wektor.erase(wektor.begin() + temp); // patrz dodatek 1

      // szukamy gdzie chcemy wstawić element
      // należy zauważyć, że i-ty element jest teraz inny
      // od tego którego wyciągnęliśmy.

      // deklarujemy j poza pętlą, by użyć go potem.
      // chcemy by j było nowym indeksem elementu
      int j;
      for (j = i; j > 0; j --) {
            // jeżeli element na pozycji j jest mniejszy od wcześniej wyjętego
            // elementu, chcemy wstawić element po nim
            if (wektor[j] < j) {
                j ++;
                break;
            }
      }

      // wstawiamy element z powrotem do wektora
      wektor.insert(wektor.begin()+j, temp); // patrz dodatek 1

    }
}
```

Podobnie jak sortowanie bąbelkowe, w najgorszym wypadku ten algorytm robi n^2/2 kroków, więc ma kompleksowość O(n^2).
Jednakowoż jest on o kilka razy szybszy od sortowania bąbelkowego. (Według moich testów około 6x szybszy)

## Sortowanie przez wybieranie

Sortowanie przez wybieranie jest, ponownie, bardziej intuicyjne dla człowieka niż komputera.
Prawdopodobnie, jeżeli sortowaliście kiedyś talię kart, użyliście tego algorytmu.

Polega on na znajdywaniu najniższego elementu który nie został jeszcze posortowany oraz umieszczenie go
po poprzednim posortowanym elementem.

Najłatwiejszym sposobem implementacji jest używanie pomocniczego wektora, do którego będziemy umieszczać
posortowane elementy.

```c++
void sortowanie_przez_wybieranie_0(vector<int> &wektor) {
    vector<int> wektor2;

    // przenosimy wszystkie elementy do drugiego wektora
    // więc wyjmujemy elementy z pierwszego wektora
    // dopóki ten jest pusty
    while(! wektor.empty() ) {
        // szukamy najmniejszego elementu w wektorze
        int fmin = 0;
        // zaczynamy od drugiego elementu
        for(int i = 1; i < wektor.size(); i++) {
            // sprawdzamy czy znaleźliśmy nowy najmniejszy element
            if (wektor[fmin] > wektor[i]) {
                fmin = i;
            }
        }
        // przekładamy najmniejszy element na koniec drugiego wektora
        wektor2.push_back(wektor[fmin]);
        wektor.erase(wektor.begin()+fmin); // patrz dodatek 1
    }
    // mamy posortowane elementy w drugim wektorze, jednak chcemy je
    // w pierwszym. Dlatego, zamieniamy je.
    wektor.swap(wektor2);
}
```

Alternatywna implementacja algorytmu, zamiast do drugiego wektora przesuwa elementy do początku wektora.

Ta metoda nie jest na tyle szybsza by miało to znaczenie (~1.6% szybsze).

```c++
void sortowanie_przez_wybieranie_1(vector<int> &wektor) {

    // j wyznacza długość posortowanej części wektora
    for(int j = 0; j < wektor.size() - 1; j++ ) {
        // szukamy najmniejszego elementu w nieposortowanej części wektora
        int fmin = j;
        // zaczynamy od drugiego elementu
        for(int i = j+1; i < wektor.size(); i++) {
            // sprawdzamy czy znaleźliśmy nowy najmniejszy element
            if (wektor[fmin] > wektor[i]) {
                fmin = i;
            }
        }
        // przekładamy najmniejszy element na początek, za poprzedni element
        int tmp = wektor[j];
        wektor[j] = wektor[fmin];
        wektor[fmin] = tmp;
    }
}
```

Ponownie, ten algorytm wykonuje, w najgorszym wypadku, n^2/2 kroków i ma kompleksowość O(n^2).
Według moich testów jest on prawie dwa razy szybszy niż sortowanie bąbelkowe, lecz ponad trzy razy wolniejszy
od sortowania przez wstawianie.

## Dodatki

Tutaj umieszczam dodatkowe informacje niepotrzebne do zrozumienia tematu,
lecz pozwalające na dodatkową optymalizację, lub głębiej wyjaśniające kod.

### Dodatek 1. `vector.beign()` i jego występowanie w `vector.insert(...)` oraz `vector.erase(...)`

`vector.begin()` zwraca wskaźnik do pierwszego elementu w wektorze.

`vector.insert(...)` i `vector.erase(...)` wymagają wskaźnika jako pierwszego elementu.
